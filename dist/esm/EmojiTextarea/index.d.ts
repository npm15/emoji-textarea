declare const Cp: ({ value, onChange, maxLength, styleCustom, isShowStatistics }: {
    value: string;
    onChange: Function;
    maxLength: number;
    styleCustom: Object;
    isShowStatistics: boolean;
}) => JSX.Element;
export default Cp;
