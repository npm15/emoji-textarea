import React, { ChangeEvent, useRef } from 'react';
import { useState, useEffect } from 'react';
import style from './index.less';
// yarn add  emoji-mart @emoji-mart/data @emoji-mart/react
import Picker from '@emoji-mart/react';
const Cp = ({
  value,
  onChange,
  maxLength,
  styleCustom,
  isShowStatistics
}: {
  value: string;
  onChange: Function;
  maxLength: number;
  styleCustom: Object;
  isShowStatistics: boolean;
}) => {
  const [content, setContent] = useState(value);
  const [existLength, setExistLength] = useState(value.length);
  useEffect(() => {
    onChange(content);
    setExistLength(content.length);
  }, [content]);
  const [isShowEmojiPane, setIsShowEmojiPane] = useState(false);
  // ①输入框文本值发生改变的回调函数
  const handleTextChange = (e: ChangeEvent) => {
    setContent((e.target as any)?.value);
  };

  const addEmoji = (elInput: any, emoji: any) => {
    // https://juejin.cn/post/7099686715547189261 https://itcn.blog/p/1928981459.html
    let startPos = elInput.selectionStart; // input 第0个字符到选中的字符
    let endPos = elInput.selectionEnd; // 选中的字符到最后的字符
    if (startPos === undefined || endPos === undefined) return;
    let txt = elInput.value;
    // 将表情添加到选中的光标位置
    let txtWithEmoji = txt.substring(0, startPos) + emoji + txt.substring(endPos);
    elInput.value = txtWithEmoji; // 赋值给input的value
    // 重新定义光标位置
    elInput.focus();
    elInput.selectionStart = startPos + emoji.length;
    elInput.selectionEnd = startPos + emoji.length;
    // 更新值
    setContent(txtWithEmoji);
  };
  // ②由于在光标中间拼接表情稍微复杂了一点，这里单独抽成 addEmoji 函数，tempTextAreaData 的更新也将在 addEmoji 中完成
  const textareaRef = useRef<any>();
  const selectEmoji = (emoji: any) => {
    let dom = textareaRef?.current; // 获取输入框的节点对象
    addEmoji(dom, emoji.native); // 将表情插入到光标后面
    setIsShowEmojiPane(false);
  };
  return (
    <div
      style={styleCustom}
      className={style.textareaBox}
      // data-statistics={`${existLength}/${maxLength}`}
    >
      <textarea
        ref={textareaRef}
        maxLength={maxLength}
        className={style.textarea}
        onChange={handleTextChange}
        value={content}
      />
      <footer className={style.footer}>
        <div className={style.emojiBox}>
          <span
            className={style.emojiTrigger}
            onClick={() => {
              setIsShowEmojiPane(!isShowEmojiPane);
            }}
          ></span>
          {isShowEmojiPane && (
            <div className={style.emojiPane}>
              <Picker
                onEmojiSelect={selectEmoji}
                navPosition="none"
                previewPosition="none"
                searchPosition="none"
                locale="zh"
              />
            </div>
          )}
        </div>
        <div className={style.statistics}>{`${existLength}/${maxLength}`}</div>
      </footer>
    </div>
  );
};
export default Cp;
