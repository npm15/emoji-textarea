function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

import React, { useRef } from 'react';
import { useState, useEffect } from 'react';
import style from "./index.less"; // yarn add  emoji-mart @emoji-mart/data @emoji-mart/react

import Picker from '@emoji-mart/react';
import { jsx as _jsx } from "react/jsx-runtime";
import { jsxs as _jsxs } from "react/jsx-runtime";

var Cp = function Cp(_ref) {
  var value = _ref.value,
      onChange = _ref.onChange,
      maxLength = _ref.maxLength,
      styleCustom = _ref.styleCustom,
      isShowStatistics = _ref.isShowStatistics;

  var _useState = useState(value),
      _useState2 = _slicedToArray(_useState, 2),
      content = _useState2[0],
      setContent = _useState2[1];

  var _useState3 = useState(value.length),
      _useState4 = _slicedToArray(_useState3, 2),
      existLength = _useState4[0],
      setExistLength = _useState4[1];

  useEffect(function () {
    onChange(content);
    setExistLength(content.length);
  }, [content]);

  var _useState5 = useState(false),
      _useState6 = _slicedToArray(_useState5, 2),
      isShowEmojiPane = _useState6[0],
      setIsShowEmojiPane = _useState6[1]; // ①输入框文本值发生改变的回调函数


  var handleTextChange = function handleTextChange(e) {
    var _e$target;

    setContent((_e$target = e.target) === null || _e$target === void 0 ? void 0 : _e$target.value);
  };

  var addEmoji = function addEmoji(elInput, emoji) {
    // https://juejin.cn/post/7099686715547189261 https://itcn.blog/p/1928981459.html
    var startPos = elInput.selectionStart; // input 第0个字符到选中的字符

    var endPos = elInput.selectionEnd; // 选中的字符到最后的字符

    if (startPos === undefined || endPos === undefined) return;
    var txt = elInput.value; // 将表情添加到选中的光标位置

    var txtWithEmoji = txt.substring(0, startPos) + emoji + txt.substring(endPos);
    elInput.value = txtWithEmoji; // 赋值给input的value
    // 重新定义光标位置

    elInput.focus();
    elInput.selectionStart = startPos + emoji.length;
    elInput.selectionEnd = startPos + emoji.length; // 更新值

    setContent(txtWithEmoji);
  }; // ②由于在光标中间拼接表情稍微复杂了一点，这里单独抽成 addEmoji 函数，tempTextAreaData 的更新也将在 addEmoji 中完成


  var textareaRef = useRef();

  var selectEmoji = function selectEmoji(emoji) {
    var dom = textareaRef === null || textareaRef === void 0 ? void 0 : textareaRef.current; // 获取输入框的节点对象

    addEmoji(dom, emoji.native); // 将表情插入到光标后面

    setIsShowEmojiPane(false);
  };

  return /*#__PURE__*/_jsxs("div", {
    style: styleCustom,
    className: style.textareaBox // data-statistics={`${existLength}/${maxLength}`}
    ,
    children: [/*#__PURE__*/_jsx("textarea", {
      ref: textareaRef,
      maxLength: maxLength,
      className: style.textarea,
      onChange: handleTextChange,
      value: content
    }), /*#__PURE__*/_jsxs("footer", {
      className: style.footer,
      children: [/*#__PURE__*/_jsxs("div", {
        className: style.emojiBox,
        children: [/*#__PURE__*/_jsx("span", {
          className: style.emojiTrigger,
          onClick: function onClick() {
            setIsShowEmojiPane(!isShowEmojiPane);
          }
        }), isShowEmojiPane && /*#__PURE__*/_jsx("div", {
          className: style.emojiPane,
          children: /*#__PURE__*/_jsx(Picker, {
            onEmojiSelect: selectEmoji,
            navPosition: "none",
            previewPosition: "none",
            searchPosition: "none",
            locale: "zh"
          })
        })]
      }), /*#__PURE__*/_jsx("div", {
        className: style.statistics,
        children: "".concat(existLength, "/").concat(maxLength)
      })]
    })]
  });
};

export default Cp;