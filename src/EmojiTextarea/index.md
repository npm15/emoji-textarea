## EmojiTextarea

Demo:

```tsx
import React from 'react';
import EmojiTextarea from 'emoji-textarea';

export default () => (
  <EmojiTextarea
    maxLength={1000}
    value="First Demo"
    onChange={(value) => console.log(value)}
    styleCustom={{ height: '200px' }}
    isShowStatistics={true}
  />
);
```

More skills for writing demo: https://d.umijs.org/guide/basic#write-component-demo
