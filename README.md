# emoji-textarea

## Getting Started

Install dependencies,

```bash
$ npm i emoji-textarea
# yarn add emoji-textarea
```

使用的时候

```jsx
import EmojiTextarea from 'emoji-textarea';

export default () => (
  <EmojiTextarea
    maxLength={1000}
    value="First Demo"
    onChange={(value) => console.log(value)}
    styleCustom={{ height: '200px' }}
    isShowStatistics={true}
  />
);
```

![emoji1](https://blog-huahua.oss-cn-beijing.aliyuncs.com/blog/code/emoji1.png)
