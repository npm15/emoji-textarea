import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import EmojiTextarea from './index';

describe('<EmojiTextarea />', () => {
  it('render EmojiTextarea with dumi', () => {
    const msg = 'dumi';

    render(<EmojiTextarea title={msg} />);
    expect(screen.queryByText(msg)).toBeInTheDocument();
  });
});
